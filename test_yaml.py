import unittest
from pprint import pprint
import yaml

class Dummy(unittest.TestCase):

    source = "dummy.yml"

    def test_load(self):
        with open(self.source) as f:
            mydict = yaml.load(f)
        pprint(mydict)
        self.assertEqual(mydict["key1"][0], "item1")


if __name__ == "__main__":
    unittest.main()
